from django.db import models


# Create your models here.
class Rate(models.Model):
    CATEGORY=[('1', 'Bad'),
            ('2', 'Poor'),
            ('3', 'Ok'),
            ('4', 'Good'),
            ('5', 'Very Good'),
            ('6', 'Excellent')]
            
    name = models.CharField(max_length=50, default='')
    forum = models.CharField(max_length=50, default='')
    star = models.CharField(max_length=50, choices=CATEGORY)
    

