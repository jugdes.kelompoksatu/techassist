from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .views import *
from django.http import HttpRequest

# Create your tests here.

class about_UnitTest(TestCase) :
    def test_about_url_is_exist(self) :
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_using_about_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_using_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, views.about)

    def test_model_rate(self):
        rate_test = Rate(
            name = "hasna",
            forum = "virus",
            star = "Good",
        ) 
        rate_test.save()
        self.assertEqual(Rate.objects.all().count(), 1)

    def test_form(self):
        form = RateForm()
        self.assertFalse(form.is_valid())


