from django import forms
from .models import Rate
from django.forms import ModelForm

class RateForm(ModelForm):
    class Meta:
        model= Rate
        fields = {'name', 'forum', 'star'}
        labels = { 'name' : 'name', 'forum' : 'forum', 'star' : 'Star'}
        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text'}),
            'forum' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text'}),
            'star' : forms.Select(attrs={'class': 'form-control'}),
        }
