from django.urls import path
from . import views

app_name = 'pageabout'

urlpatterns = [
    path('', views.about, name='about'),

    # dilanjutkan ...
]