from django.shortcuts import render
from .forms import RateForm
from .models import Rate
from django.http import HttpResponseRedirect


# Create your views here.
def about(request):
    return render(request, 'about.html')

def about(request):
    if request.method == 'POST':
        form = RateForm(request.POST)
        if form.is_valid():
            form.save()
            # return HttpResponseRedirect('../about')
    # else:
    form = RateForm()

    content = {'form' : form}

    return render(request, 'about.html', content)