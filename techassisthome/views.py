from django.shortcuts import render, redirect
from .models import PostModel
from .forms import PostForm
from django import forms

# Create your views here.
def home(request):
    post_model=PostModel.objects.all() #.order_by('-date')
    post_form = PostForm(request.POST or None) 
    error = None
    if post_form.is_valid():
        if request.method == 'POST':
            PostModel.objects.create(
                message = post_form.cleaned_data.get('message'),
                )
            return redirect('techassisthome:home')
        else:
            error = post_form.errors
    context = {
        'isian':post_form,
        'postingan':post_model,
        'navItem':'home',
        'resp':'Thanks',
    }
    return render(request, 'home.html', context)