from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .views import *
from django.http import HttpRequest

# Create your tests here.

class UnitTest(TestCase):

	def test_landingpage_uses_home_func(self):
		found = resolve('/')
		self.assertEqual(found.func, views.home)
