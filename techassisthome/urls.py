from django.urls import path
from .views import *

app_name = 'homepage'

urlpatterns = [
    path('', home, name='home'),
]
