from django import forms
from .models import PostModel

class PostForm(forms.ModelForm):
    KATEGORI = [
        ('Like', 'I Like It!'),
        ('Dislike', 'Hmm Not Yet'),
    ]
    message  = forms.ChoiceField(choices=KATEGORI, widget=forms.RadioSelect())
    class Meta:
        model = PostModel
        fields = [
            'message',
        ]