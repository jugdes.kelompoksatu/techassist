from django.db import models

# Create your models here.
class Comment(models.Model):
    name = models.CharField(max_length=50)
    comments = models.CharField(max_length=200)
