from django.test import TestCase, Client
from django.urls import resolve
from .views import isiComment
from .models import Comment
from .forms import CommentForm
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse





# Unit Test
class MyappUnitTest(TestCase):
    def test_myapp_url_is_exist(self):
        response = Client().get('/HowItWorks/')
        self.assertEqual(response.status_code, 200)

    # def test_myapp_using_landing_func(self):
    #     found = resolve('/')
    #     self.assertEqual(found.func, isiComment)

    def test_model_status(self):
        status_test = Comment(
            name = "destara",
            comments = "hai",
        ) 
        status_test.save()
        self.assertEqual(Comment.objects.all().count(), 1)


    def test_can_access_login_with_no_myapp_using_template(self):
        response = Client().get('/HowItWorks/')
        self.assertTemplateUsed(response, 'howitworks.html')

    def test_form(self):
        form = CommentForm()
        self.assertFalse(form.is_valid())


# class MyappFunctionalTest(StaticLiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(MyappFunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(MyappFunctionalTest, self).tearDown()



#     def test_howitwors(self):
#         self.browser.get(self.live_server_url + "/HowItsWorks/")

#         title = self.browser.find_element_by_tag_name('h1')
#         self.assertIn("Welcome!", title.text)
        





