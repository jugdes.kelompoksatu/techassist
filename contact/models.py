from django.db import models

# Create your models here.
class Contact(models.Model):
	yourname = models.CharField(max_length=50)
	email = models.CharField(max_length=50)
	subject = models.CharField(max_length=200)
	