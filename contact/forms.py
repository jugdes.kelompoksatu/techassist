from django import forms
from .models import Contact

class ContactForm(forms.ModelForm):
    class Meta :
        model = Contact
        fields = ('yourname','email','subject')
        labels={
                'yourname':"Your name:",
                'email':"Email:",
				'subject':"Message:",
            }