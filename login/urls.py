from django.contrib import admin
from django.urls import path
from . import views

app_name = 'login'

urlpatterns = [
    path('', views.loginpage, name='loginpage'),
    path('menu/', views.menupage, name='menupage'),
    path('logout/',views.logoutpage, name='logoutpage'),
]